function debug(){
	if [ "$debugon" == "true" ]; then
		echo $@ >&2
	fi
}

function epochTime(){
	local cDate=$1
	echo $cDate | date -f - +"%s"
}

function formatDate(){
	local unformattedDate=$1
	echo $unformattedDate | sed -E 's/(.{8})(.{4})/\1 \2/'
}

function diffInSeconds(){
	local oldDate=$1
	local newDate=$2

	debug "old date : $oldDate, new date : $newDate"

	local epochOld=$(epochTime $oldDate)
	local epochNew=$(epochTime $newDate)

	local posDiff=$((epochNew - epochOld))

	debug "$epochNew - $epochOld = $posDiff"
	echo $posDiff
}

function diffInDays(){
        local oldDate="$(formatDate $1)"
        local newDate="$(formatDate $2)"

        debug "formatted old date : $oldDate, formatted new date : $newDate"

	local diffSeconds=$(diffInSeconds "$oldDate" "$newDate")

	debug "diff in seconds is : $diffSeconds"

	local diffDays=$((diffSeconds/86400))
	debug "diff days : $diffDays"
	echo $diffDays
}


function isRdsSnapshotStale(){
	local diffDays=$1
        local acceptableDiff=$2

	debug "diff days : $diffDays, accecptable difference in days : $acceptableDiff"

	if ((diffDays > acceptableDiff)); then
		debug "$diffDays > $acceptableDiff"
		echo "true"
	else
		debug "$diffDays < $acceptableDiff"
		echo "false"
	fi
}


function getRdsDate(){
    local rdsInfo=$1
    echo $rdsInfo | rev | cut -d "-" -f1 | sed "s|\"||g" | rev
}


RUNDATE=$(date +%Y%m%d%H%M)
ACCEPTABLE_DIFF_TIME=50

jsonData=`aws elasticache describe-snapshots --region us-east-1 --query "Snapshots[?starts_with(CacheClusterId,'coll-trevecca')].SnapshotName" | grep -v "automatic"`
for redisSnap in $(echo $jsonData | sed 's/\[//g' | sed 's/\]//g' | sed 's/,/ /g' | sed 's/"/ /g')
do
    rdsDate=$(getRdsDate $redisSnap)
    debug "record : $redisSnap, run date is $RUNDATE, rds date is $rdsDate"

    daysDiff=$(diffInDays $rdsDate $RUNDATE)
    isStale=$(isRdsSnapshotStale $daysDiff $ACCEPTABLE_DIFF_TIME)

    debug "days difference is : $daysDiff, stale value is : $isStale"

    if [ "$isStale" == "true" ]; then
	    echo "Deleting record : $redisSnap, run date is $RUNDATE, redis date is $rdsDate, days difference is : $daysDiff, acceptable days difference is $ACCEPTABLE_DIFF_TIME"
	    debug "if : volume is $daysDiff days old"
           # aws elasticache delete-snapshot --region us-east-1 --snapshot-name $redisSnap
    else
	    debug "else : volume is $daysDiff days old"
    fi
    debug "-------------------------------------------------------------"
    debug ""
done
